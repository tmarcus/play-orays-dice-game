﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrayDiceGame
{
    // play Oray's dice game: http://puzzling.stackexchange.com/questions/36404/who-will-win-this-dice-game
    class Program
    {
        public static Random rand;

        static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Please provide the number of games to play.");
                return;
            }

            int NumGames;
            if (!Int32.TryParse(args[0], out NumGames))
            {
                Console.WriteLine("Invalid argument. Please provide the number of games to play.");
                return;
            }

            int Player1Wins = 0;
            int Player2Wins = 0;
            rand = new Random();
            for (int i = 1; i <= NumGames; ++i)
            {
                int result = PlayGame();
                if (result == 1) Player1Wins++;
                if (result == 2) Player2Wins++;
            }

            Console.WriteLine("Total games played: " + NumGames);
            Console.WriteLine("Player 1 wins: " + Player1Wins + " ("+ Player1Wins * 100 / (double)NumGames + "%)");
            Console.WriteLine("Player 2 wins: " + Player2Wins + " (" + Player2Wins * 100 / (double)NumGames + "%)");
        }

        public static int RollDie()
        {
            return rand.Next(6) + 1;
        }

        public static int RollDice()
        {
            return RollDie() + RollDie();
        }

        /// <summary>
        ///  Play Oray's game 1 time.
        /// </summary>
        /// <returns>1 if player 1 wins (sevens), 2 if player 2 wins (increasing)</returns>
        public static int PlayGame()
        {
            List<int> Rolls = new List<int>();

            while (true)
            {
                Rolls.Add(RollDice());
                int c = Rolls.Count;
                if (c >= 2 && Rolls[c-1] == 7 && Rolls[c-2] == 7)
                {
                    Console.WriteLine("Player 1 won after " + c + " rolls.");
                    return 1;
                }
                if (c >= 3 && Rolls[c-1] > Rolls[c-2] && Rolls[c-2] > Rolls[c-3])
                {
                    Console.WriteLine("Player 2 won after " + c + " rolls.");
                    return 2;
                }
            }
        }
    }
}
